#!/bin/sh
releaseFolder=./FacebookBanner/build/intermediates/classes/release/

class=com/mopub/mobileads/FacebookBanner.class

fullClass=$releaseFolder/$class

output=`pwd`/FacebookBanner.jar

rm -rf $output && echo "Deleted old jar file."
echo "Deleting old build -- ./FacebookBanner/build ..."
rm -f $fullClass
rm -rf ./FacebookBanner/build
echo "Building new ..."
./gradlew build -p FacebookBanner
if [ -f $fullClass ]; then
	echo "Class file exists -- $fullClass"
	echo "Creating jar"
	(cd $releaseFolder; jar cf $output $class)
	echo "Done"
else
	echo "Final class file does NOT exist -- $fullClass"
	echo "FULL FAIL."
fi


exit 0
