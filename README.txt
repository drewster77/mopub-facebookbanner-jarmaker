HOW TO BUILD THE JAR FILE:
  1.  Open a command line (terminal) and navigate to this current folder.
  2.  Run the "go" script by typing
        ./go

        This will create the FacebookBanner.jar file.
  3. Copy that .jar file to your Unity project/Assets/Plugins/Android folder.
  4. Rebuild your Unity project.

DETAILS:
Source code for FacebookBanner.java came from MoPub's public repository:

  https://github.com/mopub/mopub-android-sdk/tree/master/extras/src/com/mopub/mobileads

Source code is currently stored here:
  ./com/mopub/mobileads/FacebookBanner.java
